#!/usr/bin/env python
"""trace color images with potrace"""

# color_potrace
# Written by ukurereh
# April 21, 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# External program commands. Replace with paths to external programs as needed.
PNGNQ_PATH = 'pngnq'
IMAGEMAGICK_CONVERT_PATH = 'convert'
IMAGEMAGICK_MOGRIFY_PATH = 'mogrify'
POTRACE_PATH = 'potrace'


VERSION = '0.02w' #working on 0.02

import sys, os, subprocess, glob
import svg_stack

def runProc(*args, **kwargs):
    print('Running:', args)
    return subprocess.run(*args, **kwargs)


def quantize(src, dest_palette, numcolors, destdir, ext="~quant.png"):
    '''quantize src image to numcolors, save as same filename to destdir'''
    # build the pngnq quantize command
    command = '"{pngnq}" -f -d "{destdir}" -n {colors} -e {ext} "{src}"'.format(
        pngnq = PNGNQ_PATH, destdir=destdir, colors=numcolors, ext=ext, src=src)
    process = runProc(command, shell=True)

    # get location of the output file for palette-making
    quantized = os.path.splitext(src)[0] + ext
    quantized = os.path.join(destdir, os.path.basename(quantized))
    # build the imagemagick palette-making command
    command = '"{convert}" "{palette_src}" -unique-colors -compress none "{palette_dest}"'.format(
        convert = IMAGEMAGICK_CONVERT_PATH, palette_src=quantized, palette_dest=dest_palette)
    process = runProc(command, shell=True)


def get_ppm_colors(src):
    '''gets 3-tuple colors from an ascii ppm image (-unique-colors output)

    Assumes ImageMagick -unique-colors output, ascii, max color value 255
    '''
    with open(src, 'rt') as file:
        # skip to 4th line and get color values
        file.readline()
        file.readline()
        file.readline()
        colorvals = []
        for line in file:
            colorvals.extend([int(s) for s in line.split()])
    irange = range(0, len(colorvals), 3)
    jrange = range(3, len(colorvals)+1, 3)
    colors = []
    for i,j in zip(irange,jrange):
        colors.append(tuple(colorvals[i:j]))
    return colors


def hex_colors_str(colors):
    '''Turns list of 3-tuple colors to #rrggbb hex strings

    For use as imagemagick & potrace arguments.'''
    hex_strs = []
    for r,g,b in colors:
        hex_strs.append("#{0:02x}{1:02x}{2:02x}".format(r,g,b))
    hex_strs.reverse() #so it will go from light to dark
    return hex_strs


def isolate_color(src, dest, coloridx, palette, stacked=False):
    '''fills the specified color of src with black, all else is white

    src: source image path. had better be quantized and match palette or else
    coloridx: index of the specified color from palette
    palette: list of "#010101" etc. output from hex_colors_str
    dest: path to save output image
    stacked: if True, colors before coloridx are white, colors after are black
'''
    # copy src to dest (it will be edited in-place)
    with open(src, 'rb') as srcfile, open(dest, 'wb') as destfile:
        destfile.write(srcfile.read())

    # to avoid problems when the palette contains black or white background and,
    # foreground colors are chosen that are not in the palette, nor white or black.
    # There's probably a slightly faster way via transparency, but this works ok.
    background = get_nonpalette_color(palette, False, ["#000000", "#FFFFFF"])
    foreground = get_nonpalette_color(palette, True, [background, "#000000", "#FFFFFF"])
    for i, col in enumerate(palette):
        # fill this color with background or foreground?
        if i == coloridx:
            fill = foreground
        elif i > coloridx and stacked:
            fill = foreground
        else:
            fill = background

        # build the imagemagick command and execute it
        command = '"{mogrify}" -fill "{fill}" -opaque "{color}" "{dest}"'.format(
            mogrify = IMAGEMAGICK_MOGRIFY_PATH, fill=fill, color=col, dest=dest)
        process = runProc(command, shell=True)

    # now color the foreground black and background white
    command = '"{mogrify}" -fill "{fillbg}" -opaque "{colorbg}" -fill "{fillfg}" -opaque "{colorfg}" "{dest}"'.format(
        mogrify = IMAGEMAGICK_MOGRIFY_PATH, fillbg="#FFFFFF", colorbg=background, fillfg="#000000", colorfg=foreground, dest=dest)
    process = runProc(command, shell=True)


def get_nonpalette_color(palette, start_black=True, additional=None):
    '''return a color hex string not in palette

    additional: if specified, a list of additional colors to avoid returning'''
    if additional is None:
        palette_ = palette
    else:
        palette_ = palette + list(additional)
    if start_black:
        color_range = range(int('ffffff', 16))
    else:
        color_range = range(int('ffffff', 16), 0, -1)
    for i in color_range:
        color = "#{0:06x}".format(i)
        if color not in palette_:
            return color


def trace(src, dest, outcolor):
    '''runs potrace with default options and specified color'''
    # build the potrace command
    command = '"{potrace}" --svg --output "{dest}" --color "{outcolor}" --turdsize 2 "{src}"'.format(
        potrace = POTRACE_PATH, dest=dest, outcolor=outcolor, src=src)
    process = runProc(command, shell=True)


def color_trace_dir(srcdir, destdir, numcolors):
    '''trace all images in srcdir to numcolors, output SVGs to destdir'''
    tmp_palette = os.path.join(destdir, "~palette.ppm")
    tmp_quant   = "{0}~quant.png"
    tmp_layer   = "{0}~layer.ppm"
    tmp_trace   = "{0}~trace.svg"
    # tmp_tracen  = "{0}~trace{1}.svg"
    out_dest    = "{0}.svg"


    # check that srcdir exists
    if not os.path.isdir(srcdir):
        raise Exception("Source directory {0} does not exist".format(srcdir))

    # create destdir if it doesn't already exist
    if not os.path.exists(destdir):
        os.makedirs(destdir)

    # glob as a list so it won't include temporarily created files
    filenames = list(glob.glob(os.path.join(srcdir, "*")))
    for fname in filenames:

        # temporary and destination file paths
        rootname = os.path.basename(os.path.splitext(fname)[0])
        tmp_thisquant = os.path.join(destdir, tmp_quant.format(rootname))
        tmp_thislayer = os.path.join(destdir, tmp_layer.format(rootname))
        tmp_thistrace = os.path.join(destdir, tmp_trace.format(rootname))
        out_thisdest  = os.path.join(destdir, out_dest.format(rootname))

        tmp_thistrace = os.path.join(destdir, tmp_trace.format(rootname))
        # thistrace_n  = os.path.join(destdir, '{}-in{}.svg'.format(rootname, i))

        # quantize & make palette
        quantize(fname, tmp_palette, numcolors, destdir)
        palette = hex_colors_str(get_ppm_colors(tmp_palette))

        for i, color in enumerate(palette):
            # isolate & trace for this color
            isolate_color(tmp_thisquant, tmp_thislayer, i, palette, stacked=True)

            # tmp_thistrace = os.path.join(destdir, tmp_tracen.format(rootname, i))
            tmp_thistrace = os.path.join(destdir, tmp_trace.format(rootname))
            trace(tmp_thislayer, tmp_thistrace, color)

            out_n  = os.path.join(destdir, '{}-out{}.svg'.format(rootname, i))

            with open(tmp_thistrace, 'rb') as srcfile, open(out_n, 'wb') as destfile:
                destfile.write(srcfile.read())

            if i == 0:
                # first file in the composite svg, so copy it to dest start it off
                with open(tmp_thistrace, 'rb') as srcfile, open(out_thisdest, 'wb') as destfile:
                    destfile.write(srcfile.read())
            else:
                # layer on top of the composite svg
                doc = svg_stack.Document()
                layout = svg_stack.CBoxLayout()
                layout.addSVG(out_thisdest)
                layout.addSVG(tmp_thistrace)
                doc.setLayout(layout)
                with open(out_thisdest, 'w') as file:
                    doc.save(file)

        # delete all temporary files except palette
        #os.remove(tmp_thisquant)
        #os.remove(tmp_thislayer)
        #os.remove(tmp_thistrace)

    # delete temporary palette file
    #os.remove(tmp_palette)


def main(args):
    usage = """color_trace SRCDIR -colors n DESTDIR

trace color images with potrace

This will trace all images in SRCDIR to n colors (max 256) and output to destdir
"""
    if len(args) != 4:
        print(usage)
        sys.exit(1)

    srcdir, colorsopt, numcolors, destdir = args

    try:
        color_trace_dir(srcdir, destdir, numcolors)
    except Exception as e:
        # print("Error:", e)
        raise
        # sys.exit(2)

    sys.exit(0)

if __name__ == '__main__':
    args = sys.argv[1:]
    main(args)
